/****HEADERS****/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "op_bits.h"

/****CONSTANTES****/
#define	NEG_STATE 't' //estado opuesto
#define	TURN_OFF 'c' //interruptor apagado
#define	TURN_ON 's' //interruptor encendido
#define	SHUT_DOWN 'q' //cierre del programa

#define LED_POS A //posición de los 8 LEDs (en A ó B)

#define  ERROR -1


/****PROTOTIPOS*****/
void LEDstate(void);
void init_pins(void);
void change_value (void);
void close_pins(void);

/*****VARIABLES GLOBALES******/
char *pin[]= {"17","4","18","23","24","25","22","27"};

int main(int argc, char** argv) 
{

    init_pins(); //inicializamos los pins
    
    char c;//c: variable de buffer
    int prog = 1, comand;		//prog: estado del programa (correr o finalizar); comand: el comando ingresado
    
    /* CICLO DE PROGRAMA: OBTIENE LO INGRESADO POR CONSOLA,
       CHEQUEA LA ENTRADA VALIDA Y ACTIVA O DESACTIVA CADA BIT DEL PUERTO A
       DE ACUERDO AL COMANDO SELECCIONADO
    */
    while (prog)
    {
        LEDstate(); //imprimimos el estado de los LED en pantalla
        change_value();//cambiamos el estado de los LED en el micro
	printf("Ingrese el bit a modificar del puerto A, o un comando válido: ");
	c=getchar();
	comand=c;
	
            if (((comand>='0' && comand<= MAXBIT_AB+'0') || comand==NEG_STATE || comand==SHUT_DOWN || comand==TURN_ON || comand==TURN_OFF) && (c=getchar())=='\n')
            {
                if (comand>='0' && comand<=MAXBIT_AB+'0')
                {
                    bitSet(LED_POS ,comand-'0'); //activa el bit seleccionado
                }
                else if (comand==NEG_STATE)
                {
                   maskToggle(LED_POS, 0xFF); //cambia al estado opuesto
                   
                }
                else if (comand==TURN_OFF)
                {
                   maskOff(LED_POS, 0xFF);	//apaga el puerto completo
                   
                }
                else if (comand==TURN_ON)
                {
                  maskOn(LED_POS, 0xFF);		//enciende el puerto completo
                  
                }
                else
                {
                    printf("\nFIN DEL PROGRAMA\n");
                    prog=0;	//corta el programa
                }
             
            }
            else
            {
                printf("\nEntrada Inválida.\n\n");
                if(comand != '\n')
                {
                    while ((c=getchar())!= '\n'); //Limpiar buffer
                }
            }  
    }
    
    maskOff(LED_POS, 0xFF); //apagamos los LEDs
    change_value();//cambiamos el estado en la placa	  
    close_pins();  //desexportamos los pins
    
    return EXIT_SUCCESS;
}

/* LEDstate():
Función encargada de imprimir el estado de los 8 LEDs, según el puerto en el que se encuentren (A ó B)
No recibe ni devuelve ningún parámetro.
*/
void LEDstate(void)
{
	int i;
        printf("Estado del LED:");
        
	for (i=MAXBIT_AB;i>=0;i--)
        {
            if(i==MAXBIT_AB/2)
            {
                putchar(' '); //dejamos un espacio entre nibbles
            }
            printf("%d", bitGet(LED_POS,i));
	}
        printf("\nComandos: %c NEG_STATE | %c TURN_ON | %c TURN_OFF | %c SHUT_DOWN\n\n",NEG_STATE,TURN_ON,TURN_OFF,SHUT_DOWN);
        
}

/*INIT_PINS()
 *Función encargada de exportar los pines del microcontrolador y delcararlos como salida.
 *Recibe: no recibe parámetros (usa arreglo global de pines)
 *Devuelve:no devuelve parámetros
 */

void init_pins(void)
{
    FILE *handle; // puntero para manejar archivos
    char buffer [40];//buffer para ir manejando los distintos paths
    
    int i;
    for (i=0 ; i<=MAXBIT_AB ; i++)
    {
        if ((handle = fopen("/sys/class/gpio/export","w")) == NULL) //abrimos el archivo
        {
            printf("No se puede abirir el archivo de exp. del pin %s\n", pin[i]);
            exit(EXIT_FAILURE);
        }


        if (fputs(pin[i],handle) == ERROR)
        {
            printf ("No se pudo exportar el pin %s.\n", pin[i]);
            fclose(handle);
            exit(EXIT_FAILURE);
        }
        printf("El pin %s se exporto correctamente\n", pin[i]);
        fclose(handle);
    }

    sleep(1); //generamos un delay de 1 segundo, para que el S.O "libere" a los archivos del pin

    for (i=0 ; i<=MAXBIT_AB ; i++)
    {
        sprintf(buffer,"/sys/class/gpio/gpio%s/direction",pin[i]); //modificamos el string en el buffer con el path requerido
        if ((handle = fopen(buffer,"w")) == NULL)
        {
            printf("No se ha podido abrir el archivo direction del pin %s.\n", pin[i]);
            exit(EXIT_FAILURE);
        }


	if (fputs("out",handle) == ERROR) //declaramos al pin como salida
        {
            printf ("No se ha podido declarar al pin %s como salida.\n", pin[i]);
            fclose(handle);
            exit(EXIT_FAILURE);
	}
	else 
        {
            printf("Se ha declarado al pin %s como salida\n", pin[i]);
            fclose(handle);
	}	
    }
}

/*CHANGE_VALUE()
 *Función encargada de actualizar el estado de los pines del microcontrolador, de acuerdo a lo que ingresa el usuario.
 *Recibe: no recibe parámetros (usa arreglo global de pines)
 *Devuelve:no devuelve parámetros
 */
void change_value (void)
{
    char buffer [40]; //buffer para manejar los paths
    FILE* handle;//creamos un puntero a FILE para movernos por archivos
    
    int i;
    for (i=0 ; i<=MAXBIT_AB ; i++)
    {
        sprintf(buffer,"/sys/class/gpio/gpio%s/value",pin[i]);
	if ((handle = fopen(buffer,"w")) == NULL) //abrimos el archivo
        {
            printf("No se pudo abrir el archivo value del pin %s.\n", pin[i]);
            exit(EXIT_FAILURE);
	}
        
	if (bitGet(LED_POS, i) == HIGH) //si el LED esta encendido, ponemos un "1" en el archivo
        {
            if (fputs("1",handle)==ERROR)
            {
		printf ("No se pudo cambiar el estado del pin %s.\n", pin[i]);
		fclose(handle);
		exit(EXIT_FAILURE);
            }	
	}
        else //si esta apagado, ponemos un "0" en el archivo
        {
          if (fputs("0",handle)==ERROR)
            {
		printf ("No se pudo cambiar el estado del pin %s.\n", pin[i]);
		fclose(handle);
		exit(EXIT_FAILURE);
            }  
        }
        
        fclose(handle);//cerramos el archivo
    }
}

/*CLOSE_PINS()
 *Función encargada de desexportar los pines del microcontrolador.
 *Recibe: no recibe parámetros (usa arreglo global de pines)
 *Devuelve:no devuelve parámetros
 */
void close_pins(void)
{
    FILE *handle; // puntero para manejar archivos
    int i;
    for (i=0 ; i<=MAXBIT_AB ; i++)
    {
        if ((handle = fopen("/sys/class/gpio/unexport","w")) == NULL)
        {
            printf("No se puede abirir el archivo de unexp. del pin %s\n", pin[i]);
            exit(EXIT_FAILURE);
        }
        if (fputs(pin[i],handle) == ERROR)
        {
            printf ("No se pudo desexportar el pin %s.\n", pin[i]);
            fclose(handle);
            exit(EXIT_FAILURE);
        }
        printf("El pin %s se desexportó correctamente\n", pin[i]);
        fclose(handle);
    }
}










